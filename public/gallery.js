const expandThumbnailImage = (src, title) => {
  // Get the expanded image
  var expandImg = document.getElementById("expandedImg");
  // Get the image text
  var imgText = document.getElementById("imgtext");
  // Use the same src in the expanded image as the image being clicked on from
  // the grid
  expandImg.src = src;
  // Use the value of the alt attribute of the clickable image as text inside
  // the expanded image
  imgText.innerHTML = title;
  // Show the container element (hidden with CSS)
  expandImg.parentElement.style.display = "block";
};

const addPrefetch = imgName => {
  const link = document.createElement("link");
  link.href = `images/${imgName}`;
  link.rel = "prefetch";
  link.id = `prefetch-${imgName}`;

  document.head.appendChild(link);
};

const displayRecipeLink = (hash) => {
  const linkContainer = document.querySelector('#recipeLink');
  if (hash) {    
    const link = linkContainer.querySelector('a')
    linkContainer.setAttribute('class', '' );
    link.setAttribute('href', `recipes.html#${hash}`)
  } else {
    linkContainer.setAttribute('class', 'hidden');
  }
}

document.getElementById("thumbnailRow").addEventListener("click", ev => {
  expandThumbnailImage(
    `images/${ev.target.getAttribute("data-image-name")}`,
    ev.target.getAttribute("alt")
  );
  displayRecipeLink(ev.target.getAttribute('data-recipe-hash'));
});

let hoverRecord = {};

[...document.getElementById("thumbnailRow").querySelectorAll(".thumbnail")].forEach(
  thumbnailRef => {
    thumbnailRef.addEventListener("mouseenter", ev => {
      const imgName = ev.target.getAttribute("data-image-name");
      console.log(`${imgName} enter`);
      if (!document.getElementById(`prefetch-${imgName}`)) {
        hoverRecord[imgName] = true;
        setTimeout(
          (savedImgName => () => {
              console.log('hoverRecord', savedImgName, hoverRecord)
            if (hoverRecord[savedImgName]) addPrefetch(savedImgName);
          })(imgName),
          200
        );
      }
    });
    thumbnailRef.addEventListener("mouseleave", ev => {
        console.log(`${ev.target.getAttribute("data-image-name")} leave`)
      delete hoverRecord[ev.target.getAttribute("data-image-name")];
    });
  }
);

document.querySelector('.imgContainer > .closebtn')
  .addEventListener('click', ev => {
    ev.target.parentElement.style.display='none';
    displayRecipeLink(false);
  })